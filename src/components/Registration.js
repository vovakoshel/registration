import React, { Component } from "react";

class Registration extends Component {
  state = {
    firtsName: "",
    lastName: "",
    login: "",
    password: "",
    email: ""
  };

  onChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  onSubmit = e => {
    const { firtsName, lastName, login, password, email } = this.state;
    e.preventDefault();

    fetch("http://localhost:8080/Blog/Registration", {
      method: "POST",
      body: JSON.stringify({
        name: firtsName,
        surname: lastName,
        login: login,
        password: password,
        mail: email
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    this.setState({
      firtsName: "",
      lastName: "",
      login: "",
      password: "",
      email: ""
    });
  };

  render() {
    const { firtsName, lastName, login, password, email } = this.state;
    return (
      <div className="mt-5">
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            {" "}
            <input
              type="text"
              name="firtsName"
              onChange={this.onChange}
              value={firtsName}
              placeholder="FirstName"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="lastName"
              onChange={this.onChange}
              value={lastName}
              placeholder="LastName"
              className="form-control"
            />
          </div>
          <div className="form-group">
            {" "}
            <input
              type="text"
              name="login"
              onChange={this.onChange}
              value={login}
              placeholder="Login"
              className="form-control"
            />
          </div>
          <div className="form-group">
            {" "}
            <input
              type="password"
              name="password"
              onChange={this.onChange}
              value={password}
              placeholder="Password"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <input
              type="email"
              name="email"
              onChange={this.onChange}
              value={email}
              placeholder="Email"
              className="form-control"
            />
          </div>

          <input
            type="submit"
            value="Sign Up"
            className="btn btn-primary btn-block"
          />
        </form>
      </div>
    );
  }
}
export default Registration;
