import React, { Component } from "react";

class Navbar extends Component {
  render() {
    return (
      <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="/">
          CapitalLetter
        </a>
      </nav>
    );
  }
}
export default Navbar;
