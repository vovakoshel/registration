import React, { Component } from "react";
import Navbar from "./components/layout/Navbar";
import Registration from "./components/Registration";

import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <div className="container">
          <Registration />
        </div>
      </div>
    );
  }
}

export default App;
